package ic.android.util.barcodes


import android.graphics.Bitmap
import android.graphics.Color

import ic.android.graphics.bitmap.copyCrop

import com.google.zxing.BarcodeFormat
import com.google.zxing.oned.EAN13Writer


internal fun encodeStringToEan13BarcodeBitmap (string: String) : Bitmap {

	val bitMatrix = EAN13Writer().encode(string, BarcodeFormat.EAN_13, 1000, 250)

	val bitmap = Bitmap.createBitmap(
		bitMatrix.width,
		bitMatrix.height,
		Bitmap.Config.RGB_565
	)

	for (x in 0 until bitMatrix.width) {
		for (y in 0 until bitMatrix.height) {
			bitmap.setPixel(x, y, if (bitMatrix[x, y]) Color.BLACK else Color.WHITE)
		}
	}

	return bitmap.copyCrop(72, 0, bitmap.width - 72 * 2, bitmap.height)

}