package ic.android.util.barcodes


sealed class BarcodeFormat {

	object Pdf417 : BarcodeFormat()

	object Ean13 : BarcodeFormat()

	object Qr : BarcodeFormat()

}