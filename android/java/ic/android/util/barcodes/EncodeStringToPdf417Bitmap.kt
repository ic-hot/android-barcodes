package ic.android.util.barcodes


import android.graphics.Bitmap
import android.graphics.Color

import ic.android.graphics.bitmap.copyCropSymmetrically

import com.google.zxing.pdf417.PDF417Writer
import com.google.zxing.BarcodeFormat


internal fun encodeStringToPdf417BarcodeBitmap (string: String) : Bitmap {

	val bitMatrix = PDF417Writer().encode(string, BarcodeFormat.PDF_417, 1000, 250)

	val bitmap = Bitmap.createBitmap(
		bitMatrix.width,
		bitMatrix.height,
		Bitmap.Config.RGB_565
	)

	for (x in 0 until bitMatrix.width) {
		for (y in 0 until bitMatrix.height) {
			bitmap.setPixel(x, y, if (bitMatrix[x, y]) Color.BLACK else Color.WHITE)
		}
	}

	return bitmap.copyCropSymmetrically(31)

}