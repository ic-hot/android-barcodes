package ic.android.util.barcodes


import android.graphics.Bitmap


fun encodeStringToBarcodeBitmap (string: String, barcodeFormat: BarcodeFormat) : Bitmap {

	return when (barcodeFormat) {

		BarcodeFormat.Pdf417 -> encodeStringToPdf417BarcodeBitmap(string)
		BarcodeFormat.Ean13  -> encodeStringToEan13BarcodeBitmap(string)
		BarcodeFormat.Qr     -> encodeStringToQrCodeBitmap(string)

	}

}