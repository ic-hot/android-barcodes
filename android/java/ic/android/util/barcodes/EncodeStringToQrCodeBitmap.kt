package ic.android.util.barcodes


import android.graphics.Bitmap
import android.graphics.Color

import ic.android.graphics.bitmap.copyCropSymmetrically

import com.google.zxing.BarcodeFormat
import com.google.zxing.qrcode.QRCodeWriter


internal fun encodeStringToQrCodeBitmap (string: String) : Bitmap {

	val bitMatrix = QRCodeWriter().encode(string, BarcodeFormat.QR_CODE, 512, 512)

	val bitmap = Bitmap.createBitmap(
		bitMatrix.width,
		bitMatrix.height,
		Bitmap.Config.RGB_565
	)

	for (x in 0 until bitMatrix.width) {
		for (y in 0 until bitMatrix.height) {
			bitmap.setPixel(x, y, if (bitMatrix[x, y]) Color.BLACK else Color.WHITE)
		}
	}

	return bitmap.copyCropSymmetrically(70)

}